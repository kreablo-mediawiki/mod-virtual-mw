AC_INIT(mod_virtual_mw, 0.1-SNAPSHOT, andreas.jonsson@kreablo.se)
AC_COPYRIGHT([
Copyright 2012  Andreas Jonsson

This file is part of mod_virtual_mw.

Libwikimodel is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
])


AM_INIT_AUTOMAKE
AC_PROG_CC
AC_PROG_LIBTOOL
AC_CONFIG_MACRO_DIRS([m4])


AC_HAVE_FUNCS(mmap munmap shm_open shm_unlink)


APR_FIND_APR([], [], [1], [1])

# Determine Apache version and find apxs

AX_WITH_APXS([apxs2])

if test -n "$APXS"; then
    APACHE_VERSION=`\`$APXS -q SBINDIR\`/\`$APXS -q TARGET\` -v \
                    | grep "Server version" \
                    | cut -f2 -d":" \
                    | cut -f2 -d"/" \
                    | cut -f1 -d" "`
    major_version=`echo $APACHE_VERSION|cut -f1,2 -d.`
    if test "$major_version" = "2.0" -o "$major_version" = "2.2"; then
      APACHE_VERSION_2=true
      APACHE_VERSION_1_3=false
    else
      APACHE_VERSION_2=false
      APACHE_VERSION_1_3=true
    fi
    AC_SUBST(APACHE_VERSION_1_3)
    AC_SUBST(APACHE_VERSION_2)
    
    APXS_INCLUDEDIR=`$APXS -q INCLUDEDIR`
    APXS_LIBEXECDIR=`$APXS -q LIBEXECDIR`
    if test x"${APACHE_VERSION_2}" = xtrue ; then \
      APXS_EXTRA_CFLAGS=`$APXS -q EXTRA_CFLAGS`
    fi
    if test x"${APACHE_VERSION_1_3}" = xtrue; then \
      APXS_EXTRA_CFLAGS=`$APXS -q CFLAGS`
    fi
    AC_SUBST([APXS_INCLUDEDIR])
    AC_SUBST([APXS_LIBEXECDIR])
    AC_SUBST([APXS_EXTRA_CFLAGS])
else
    APXS="/notfound/"
    AC_SUBST(APXS)
fi

if test x"${apr_found}" = xyes ; then \
    APR_INCLUDEDIR=`${apr_config} --includedir`
    AC_SUBST([APR_INCLUDEDIR])
    APR_CPPFLAGS=`${apr_config} --cppflags`
    AC_SUBST([APR_CPPFLAGS])
    APR_LDFLAGS=`${apr_config} --ldflags`
    AC_SUBST([APR_LDFLAGS])
    APR_CFLAGS=`${apr_config} --cflags`
    AC_SUBST([APR_CFLAGS])
    APR_LINKLD=`${apr_config} --link-ld`
    AC_SUBST([APR_LINKLD])
else
    AC_MSG_ERROR([APR is needed to build.])
fi

AH_TEMPLATE([SITE_TABLE_FILENAME], [The filename of the site table yaml file.])

AC_ARG_WITH([site-table-filename],
            [AS_HELP_STRING([--with-site-table-filename=<filename>], [File name of site table yaml file.])],
	    [AC_DEFINE([SITE_TABLE_FILENAME], "$with_site_table_filename")
	     AC_SUBST([SITE_TABLE_FILENAME], "$with_site_table_filename")],
	    [AC_DEFINE([SITE_TABLE_FILENAME], "/srv/virtual_mw/site_table.yaml")
	     AC_SUBST([SITE_TABLE_FILENAME], "/srv/virtual_mw/site_table.yaml")])

YAML_REQUIRE()

AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([Makefile])
AC_OUTPUT
