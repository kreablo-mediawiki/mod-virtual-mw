/*
 * Copyright 2012  Andreas Jonsson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <httpd.h>
#include <http_config.h>
#include <http_request.h>
#include <http_log.h>
#include <apr.h>
#include <apr_strings.h>
#include <apr_optional.h>
#include <apr_tables.h>
#include <mod_rewrite.h>

#undef PACKAGE_BUGREPORT
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION

#include "config.h"

#include "site_table.h"
#include "check_speling.h"

#define MOD_VIRTUAL_SITE_ENV_VARIABLE "VIRTUAL_MW_SITE"
#define MOD_VIRTUAL_VERSION_ENV_VARIABLE "VIRTUAL_MW_VERSION"

static void register_hooks(apr_pool_t * pool);
static void instantiate_site_table(apr_pool_t * pchild, server_rec *s);

static char * article_path_rewrite_mapfunc(request_rec *r, char * key);
static char * site_name_rewrite_mapfunc(request_rec *r, char * key);
static char * script_path_rewrite_mapfunc(request_rec *r, char * key);
static char * version_rewrite_mapfunc(request_rec *r, char * key);
static char * alias_rewrite_mapfunc(request_rec *r, char * key);
static char * fpm_address_rewrite_mapfunc(request_rec *r, char * key);
static char * select_rewrite_mapfunc(request_rec *r, char * key);
static char * escape_ampersand_and_questionmark(request_rec *r, char * key);
static const site_t * lookup_path_resolve_alias(const request_rec * r, const char *key);

module AP_MODULE_DECLARE_DATA   virtual_mw_module =
{
    STANDARD20_MODULE_STUFF,
    NULL,            /* Per-directory configuration handler */
    NULL,            /* Merge handler for per-directory configurations */
    NULL,            /* Per-server configuration handler */
    NULL,            /* Merge handler for per-server configurations */
    NULL,            /* Any directives we may have for httpd */
    register_hooks   /* Our hook registering function */
};

static site_table_t * site_table = NULL;

static void register_hooks(apr_pool_t * pool)
{
    APR_OPTIONAL_FN_TYPE(ap_register_rewrite_mapfunc) *register_rewrite_mapfunc
        = APR_RETRIEVE_OPTIONAL_FN(ap_register_rewrite_mapfunc);

    if (register_rewrite_mapfunc == NULL) {
        ap_log_perror(APLOG_MARK, APLOG_ERR, 0, pool, "mod_rewrite appears to be disabled!");
        return;
    }

    ap_hook_child_init(instantiate_site_table, NULL, NULL, APR_HOOK_FIRST);

    register_rewrite_mapfunc("virtual_mw_site_name", site_name_rewrite_mapfunc);
    register_rewrite_mapfunc("virtual_mw_version", version_rewrite_mapfunc);
    register_rewrite_mapfunc("virtual_mw_fpm_address", fpm_address_rewrite_mapfunc);
    register_rewrite_mapfunc("virtual_mw_alias_of", alias_rewrite_mapfunc);
    register_rewrite_mapfunc("virtual_mw_select", select_rewrite_mapfunc);
    register_rewrite_mapfunc("virtual_mw_script_path", script_path_rewrite_mapfunc);
    register_rewrite_mapfunc("virtual_mw_article_path", article_path_rewrite_mapfunc);
    register_rewrite_mapfunc("escape_ampersand_and_questionmark", escape_ampersand_and_questionmark);
}


static void instantiate_site_table(apr_pool_t * pchild, server_rec *s)
{
    site_table = site_table_initialize(pchild, s);
}

/**
 * Select a path for further processing.
 *
 * @param r The request record.
 * @param key The key to lookup.  Should be a uri fragment.
 *
 * @return a string on any of the following forms:
 *            article/$1
 *            script/$1
 *            redirect/<url>
 *            non_existing/$1
 */
static char * select_rewrite_mapfunc(request_rec * r, char * key)
{
    const site_t * site = site_table_lookup_path(site_table, key, r);

    if (site == NULL && r->method_number == M_GET) {

        const char * new_path;

        switch (check_speling(r, site_table, key, &new_path)) {
            case OK:
                return apr_pstrcat(r->pool, "redirect/", new_path, NULL);
        }

        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Non-existing path [%s].", key);
        return apr_pstrcat(r->pool, "non_existing/", key, NULL);
    }

    if (strcmp(key, site->script_path) == 0) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "The path [%s] is the script path of [%s].", key, site->name);
        return apr_pstrcat(r->pool, "script/", key, NULL);
    }

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "The path [%s] is the article path of [%s].", key, site->name);
    return apr_pstrcat(r->pool, "article/", key, NULL);

}

/**
 * Rewrite map function for site name.
 *
 * @param r The request record.
 * @param key The key to lookup.  Should be a site name.
 * @param return the site name of a site.
 */
static char * site_name_rewrite_mapfunc(request_rec * r, char * key)
{
    const site_t * site = site_table_lookup_path(site_table, key, r);

    if (site != NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Site name for path [%s] is [%s].", key, site->name);
        return (char *) site->name;
    }

    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "The site [%s] cannot be lookuped up!", key);

    return "";
}

/**
 * Rewrite map function for script path.
 *
 * @param r The request record.
 * @param key The key to lookup.  Should be a site name.
 * @param return the script path of a site.
 */
static char * script_path_rewrite_mapfunc(request_rec * r, char * key)
{
    const site_t * site = site_table_lookup_path(site_table, key, r);

    if (site != NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Script path of [%s] is [%s].", key, site->script_path);
        return (char *) site->script_path;
    }

    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "The site [%s] cannot be lookuped up!", key);

    return "";
}

/**
 * Rewrite map function for article path.
 *
 * @param r The request record.
 * @param key The key to lookup.  Should be a site name.
 * @param return the article path of a site.
 */
static char * article_path_rewrite_mapfunc(request_rec * r, char * key)
{
    const site_t * site = site_table_lookup_path(site_table, key, r);

    if (site != NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Article path of [%s] is [%s].", key, site->article_path);
        return (char *) site->article_path;
    }

    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "The site [%s] cannot be lookuped up!", key);

    return "";
}

/**
 * Rewrite map function for version.
 *
 * @param r The request record.
 * @param key The key to lookup.  Should be a site name.
 * @param return the version of a site.
 */
static char * version_rewrite_mapfunc(request_rec * r, char * key)
{
    const site_t * site = lookup_path_resolve_alias(r, key);

    if (site != NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Version of [%s] is [%s].", key, site->version);
        return (char *) site->version;
    }

    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "The site [%s] cannot be looked up!", key);

    return "";
}

/**
 * Rewrite map function for aliased site.
 *
 * @param r The request record.
 * @param key The key to lookup.  Should be a site name.
 * @return the site name of the aliased site, or the site name of the
 * site itself if not an alias.  Empty string if site doesn't exist.
 */
static char * alias_rewrite_mapfunc(request_rec * r, char * key)
{
    const site_t * site = site_table_lookup_path(site_table, key, r);

    if (site != NULL) {
        if (site->alias_of != NULL) {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "The site [%s] is an alias of [%s].", site->name, site->alias_of);
            return (char *) site->alias_of;
        }
        return (char *) site->name;
    }

    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "The site [%s] cannot be looked up!", key);

    return "";
}

/**
 * Rewrite map function for fpm address.
 *
 * @param r The request record.
 * @param key The key to lookup.  Should be a site name.
 * @param return the fpm address of the site site.
 */
static char * fpm_address_rewrite_mapfunc(request_rec * r, char * key)
{
    const site_t * site = lookup_path_resolve_alias(r, key);

    if (site != NULL) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "The site [%s] has fpm address [%s].", site->name, site->fpm_address);
        return (char *) site->fpm_address;
    }

    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "The site [%s] cannot be looked up!", key);

    return "";
}

static const site_t * lookup_path_resolve_alias(const request_rec * r, const char *key)
{
    const site_t * site = site_table_lookup_path(site_table, key, r);

    if (site->alias_of != NULL) {
        const site_t * target = site_table_lookup_site(site_table, site->alias_of);
        if (target == NULL) {
            ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "Target [%s] of alias [%s] does not exist!", site->alias_of, key );
            return NULL;
        } else if (target->alias_of != NULL) {
            ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "Alias [%s] of alias [%s] is not supported for site  '%s'.", key, site->alias_of, site->name);
            return NULL;
        }

        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Site [%s] is an alias of [%s].", key, site->alias_of);

        return target;
    }

    return site;
}

/**
 * Escape '?' and '&' in the given string using URL-encoding ("%26" and "%3F").
 *
 * @param r The request.
 * @param key The string to escape.
 * @return The escaped string.
 */
static char * escape_ampersand_and_questionmark(request_rec * r, char *key)
{
    const char *cur_start = key;
    const char **segment;
    const char *p;
    apr_array_header_t *segments = apr_array_make(r->pool, 0, sizeof(char *));

#define add(s) segment = apr_array_push(segments); *segment = (s);

    for (p = key; *p != '\0'; p++) {

        const char * s = NULL;

        switch (*p) {
            case '&':
                s = "%26";
                break;
            case '?':
                s = "%3F";
                break;
        }

        if (s) {
            add(apr_pstrndup(r->pool, cur_start, p - cur_start));
            add(s);
            cur_start = p + 1;
        }

    }

    add(cur_start);

#undef add

    return apr_array_pstrcat(r->pool, segments, '\0');

}
