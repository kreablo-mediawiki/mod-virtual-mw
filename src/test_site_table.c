/*
 * Copyright 2012  Andreas Jonsson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>

#include "site_table.h"

static void site_table_destroy(void);

int main(int argc, char *const* argv)
{
    if (apr_initialize()) {
        fprintf(stderr, "Failed to initialize apr.\n");
        return -1;
    }
    if (atexit(site_table_destroy)) {
        fprintf(stderr, "Failed to initiate atexit callback.");
        return -1;
    }

    apr_pool_t * pool;

    if (apr_pool_create_core(&pool)) {
        fprintf(stderr, "Faile to initiate an apr pool.");
        return -1;
    }

    site_table_t * site_table = site_table_initialize(pool);

    if (site_table == NULL) {
        fprintf(stderr, "Failed to initialize site table.\n");
        return -1;
    }

    printf("kreablo mapping: %s\n", site_table_lookup_domain_name(site_table, "kreablo.se"));

    return 0;
}

static void site_table_destroy()
{
    apr_terminate();
}

