/*
 * Copyright 2012  Andreas Jonsson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <apr.h>
#include <apr_general.h>
#include <apr_shm.h>
#include <apr_strings.h>
#include <apr_hash.h>
#include <apr_thread_proc.h>
#include <apr_thread_rwlock.h>
#include <stdlib.h>
#include <yaml.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <httpd.h>
#include <http_log.h>

#undef PACKAGE_BUGREPORT
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION


#include "config.h"
#include "site_table.h"


/**
 * The data type of the site table.
 */
struct site_table
{
    apr_pool_t * pool;
    apr_pool_t * subpool;

    apr_hash_t * path_table;

    apr_hash_t * site_configurations;
    apr_thread_rwlock_t * lock;
    apr_thread_t * update_thread;
    server_rec * s;
    int          fd;
    FILE *       file;
    time_t       modification_time;
};

typedef enum parser_state
{
    STATE_INITIAL,
    STATE_STREAM,
    STATE_SITE_NAME,
    STATE_SITE_CONTENT,
    STATE_SITE_PARAMETER_KEY,
    STATE_SITE_PARAMETER_VALUE,
} parser_state_t;

static int init_site_table(site_table_t *site_table);
static int read_site_table(site_table_t *site_table);
static void * maintenance_thread(apr_thread_t * thread, void * data);
static int check_path_exists(const site_table_t *site_table, const site_t *site);

static const char * site_key(apr_pool_t *pool, const char *key, const char *hostname)
{
    const int len = strlen(key) + (hostname == NULL ? 0 : strlen(hostname) + 1);

    char * key0 = apr_pcalloc(pool, len + 1);

    strcpy(key0, key);

    if (hostname != NULL) {
        strncat(key0, ":", len + 1);
        strncat(key0, hostname, len + 1);
    }

    return key0;
}

site_table_t * site_table_initialize(apr_pool_t * apr_pool, server_rec *s)
{
    apr_status_t status;

    site_table_t * site_table = apr_pcalloc(apr_pool, sizeof *site_table);

    if (site_table == NULL) {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "Failed to allocate memory for site table.");
        return NULL;
    }

    site_table->pool = apr_pool;

    site_table->subpool = NULL;

    site_table->site_configurations = apr_hash_make(apr_pool);

    site_table->path_table = apr_hash_make(apr_pool);

    site_table->s = s;

    status = apr_thread_rwlock_create(&site_table->lock, apr_pool);

    if (status) {
        ap_log_error(APLOG_MARK, APLOG_ERR, status, s, "Failed to create lock for site table.");
        return NULL;
    }

    if (init_site_table(site_table)) {
        return NULL;
    }

    apr_threadattr_t * thread_attr;

    status = apr_threadattr_create(&thread_attr, apr_pool);

    if (status) {
        ap_log_error(APLOG_MARK, APLOG_ERR, status, s, "Failed to create attributes for maintenance thread.");
        return NULL;
    }

    apr_threadattr_detach_set(thread_attr, 1);

    status = apr_thread_create(&site_table->update_thread, thread_attr, maintenance_thread, site_table, apr_pool);

    if (status) {
        ap_log_error(APLOG_MARK, APLOG_ERR, status, s, "Failed to start maintenance thread.");
        return NULL;
    }

    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, s, "Successfully initated site table.");

    return site_table;
}


static int init_site_table(site_table_t * site_table)
{
    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, site_table->s, "Initiating site table.");

    site_table->fd = open(SITE_TABLE_FILENAME, O_RDONLY);
    if (site_table->fd == -1) {
        ap_log_error(APLOG_MARK, APLOG_ERR, APR_FROM_OS_ERROR(errno), site_table->s, "Failed to open %s", SITE_TABLE_FILENAME);
        return -1;
    }

    site_table->file = fdopen(site_table->fd, "rb");
    if (site_table->file == NULL) {
        ap_log_error(APLOG_MARK, APLOG_ERR, APR_FROM_OS_ERROR(errno), site_table->s, "Failed to open %s", SITE_TABLE_FILENAME);
        return -1;
    }

    return read_site_table(site_table);
}

static int read_site_table(site_table_t * site_table)
{
    int ret = -1;

    yaml_parser_t parser;

    /* Create the Parser object. */
    yaml_parser_initialize(&parser);

    ret = flock(site_table->fd, LOCK_SH);
    if (ret == -1) {
        ap_log_error(APLOG_MARK, APLOG_ERR, APR_FROM_OS_ERROR(errno), site_table->s,
                     "Failed to lock file");
    }

    FILE *input = site_table->file;

    rewind(input);

    yaml_parser_set_input_file(&parser, input);

    yaml_event_t event;

    struct site * site = NULL;

    int done = 0;
    parser_state_t state = STATE_INITIAL;
    parser_state_t expect_state = STATE_SITE_NAME;
    int got_site_name = 0;
    int got_site_content = 0;

    const char * key = NULL;
    const char ** value = NULL;

    apr_status_t status = apr_thread_rwlock_wrlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_wrlock(site_table->lock);
    }

    apr_hash_clear(site_table->path_table);

    apr_hash_clear(site_table->site_configurations);

    if (site_table->subpool != NULL) {
        apr_pool_destroy(site_table->subpool);
    }

    apr_pool_create(&site_table->subpool, site_table->pool);

    apr_pool_t * subpool = site_table->subpool;

    while (!done) {
        /* Get the next event. */
        if (!yaml_parser_parse(&parser, &event)) {
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                         "Failed to parse %s", SITE_TABLE_FILENAME);
            goto error;
        }

        switch (state) {
        case STATE_INITIAL:
            if (event.type == YAML_STREAM_START_EVENT) {
                state = STATE_STREAM;
            } else {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                             "Failed to parse %s, unexpected event type: %d",
                             SITE_TABLE_FILENAME, event.type);
                goto error;
            }
            break;
        case STATE_STREAM:
            if (event.type == YAML_STREAM_END_EVENT) {
                state = STATE_INITIAL;
                done = 1;
            } else if (event.type == YAML_DOCUMENT_START_EVENT) {
                state = expect_state;
                if (expect_state == STATE_SITE_NAME) {
                    got_site_content = 0;
                    expect_state = STATE_SITE_CONTENT;
                } else {
                    got_site_name = 0;
                    expect_state = STATE_SITE_NAME;
                }
            } else {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                             "Failed to parse %s, unexpected event type: %d",
                             SITE_TABLE_FILENAME, event.type);
                goto error;
            }
            break;
        case STATE_SITE_NAME:
            if (event.type == YAML_SCALAR_EVENT && !got_site_name) {
                got_site_name = 1;
                site = apr_pcalloc(subpool, sizeof(*site));
                site->script_path = NULL;
                site->article_path = NULL;
                site->hostname = NULL;
                site->version = NULL;
                site->alias_of = NULL;
                site->fpm_address = NULL;
                site->name = apr_pstrndup(subpool, (char *) event.data.scalar.value, event.data.scalar.length);
            } else if (event.type == YAML_DOCUMENT_END_EVENT && got_site_name) {
                state = STATE_STREAM;
            } else {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                             "Failed to parse %s, unexpected event type: %d",
                             SITE_TABLE_FILENAME, event.type);
                goto error;
            }
            break;
        case STATE_SITE_CONTENT:
            if (event.type == YAML_DOCUMENT_END_EVENT && got_site_content) {

                int do_add = 1;

                if (site->article_path == NULL) {
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                                 "Invalid site definition for %s, article path missing!", site->name);
                    do_add = 0;
                }
                if (site->script_path == NULL) {
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                                 "Invalid site definition for %s, script path missing!", site->name);

                    do_add = 0;
                }
                if (strcmp(site->article_path, site->script_path) == 0) {
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                                 "Article path and script path mustn't be the same for site [%s]!", site->name);
                    do_add = 0;
                }
                if (site->version == NULL && site->alias_of == NULL) {
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                                 "Invalid site definition for %s, version is missing!", site->name);

                    do_add = 0;
                }
                if (site->version != NULL && site->alias_of != NULL) {
                    ap_log_error(APLOG_MARK, APLOG_WARNING, 0, site_table->s,
                                 "Will ignore version %s of alias site %s!", site->version, site->name);
                }
                if (site->fpm_address != NULL && site->alias_of != NULL) {
                    ap_log_error(APLOG_MARK, APLOG_WARNING, 0, site_table->s,
                                 "Will ignore fpm address %s of alias site %s!", site->fpm_address, site->name);
                }

                if (do_add) {
                    apr_hash_set(site_table->site_configurations, site->name, APR_HASH_KEY_STRING, site);
                }

                state = STATE_STREAM;
            } else if (event.type == YAML_MAPPING_START_EVENT && !got_site_content) {
                state = STATE_SITE_PARAMETER_KEY;
                got_site_content = 1;
            } else {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                             "Failed to parse %s, unexpected event type: %d",
                             SITE_TABLE_FILENAME, event.type);
                goto error;
            }
            break;
        case STATE_SITE_PARAMETER_KEY:
            if (event.type == YAML_MAPPING_END_EVENT) {
                state = STATE_SITE_CONTENT;
            } else if (event.type == YAML_SCALAR_EVENT) {
                state = STATE_SITE_PARAMETER_VALUE;
                key = apr_pstrndup(subpool, (char *) event.data.scalar.value, event.data.scalar.length);
                if (strcmp(key, "article_path") == 0) {
                    value = &site->article_path;
                } else if (strcmp(key, "script_path") == 0) {
                    value = &site->script_path;
                } else if (strcmp(key, "alias_of") == 0) {
                    value = &site->alias_of;
                } else if (strcmp(key, "version") == 0) {
                    value = &site->version;
                } else if (strcmp(key, "fpm_address") == 0) {
                    value = &site->fpm_address;
                } else if (strcmp(key, "hostname") == 0) {
                    value = &site->hostname;
                } else {
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                                 "Failed to parse %s, invalid site property: %s",
                                 SITE_TABLE_FILENAME, key);
                    goto error;
                }
            } else {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                             "Failed to parse %s, unexpected event type: %d",
                             SITE_TABLE_FILENAME, event.type);
                goto error;
            }
            break;
        case STATE_SITE_PARAMETER_VALUE:
            if (event.type == YAML_SCALAR_EVENT) {
                state = STATE_SITE_PARAMETER_KEY;
                *value = apr_pstrndup(subpool, (char *) event.data.scalar.value, event.data.scalar.length);
            } else  {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s,
                             "Failed to parse %s, unexpected event type: %d",
                             SITE_TABLE_FILENAME, event.type);
                goto error;
            }
            break;
        }

        /* The application is responsible for destroying the event object. */
        yaml_event_delete(&event);
    }

    struct stat stat;
    ret = fstat(site_table->fd, &stat);

    if (ret == -1) {
        ap_log_error(APLOG_MARK, APLOG_ERR, APR_FROM_OS_ERROR(errno), site_table->s, "Failed to stat site table file");
        goto error;
    }

    site_table->modification_time = stat.st_mtime;

    apr_hash_index_t *hi = apr_hash_first(subpool, site_table->site_configurations);

    for (; hi; hi = apr_hash_next(hi)) {
        const site_t * site;
        void * ret;

        apr_hash_this(hi, NULL, NULL, &ret);

        site = ret;

        if (!check_path_exists(site_table, site)) {
            apr_hash_set(site_table->path_table, site_key(subpool, site->script_path, site->hostname), APR_HASH_KEY_STRING, site);
            apr_hash_set(site_table->path_table, site_key(subpool, site->article_path, site->hostname), APR_HASH_KEY_STRING, site);
        }
    }

    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, site_table->s, "Successfully re-read site table.");

    ret = 0;

 error:

    ret = flock(site_table->fd, LOCK_UN);
    if (ret == -1) {
        ap_log_error(APLOG_MARK, APLOG_ERR, APR_FROM_OS_ERROR(errno), site_table->s,
                     "Failed to unlock file");
    }

    status = apr_thread_rwlock_unlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_unlock(site_table->lock);
    }

    /* Destroy the Parser object. */
    yaml_parser_delete(&parser);

    return ret;
}

int site_table_read_lock(site_table_t * site_table)
{
    apr_status_t status;

    status = apr_thread_rwlock_rdlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_rdlock(site_table->lock);
    }

    return 0;
}

int site_table_read_unlock(site_table_t * site_table)
{
    apr_status_t status;

    status = apr_thread_rwlock_unlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_unlock(site_table->lock);
    }

    return 0;
}

apr_hash_index_t * site_table_site_iterator(apr_pool_t * pool, site_table_t * site_table)
{
    return apr_hash_first(pool, site_table->site_configurations);
}

static void * maintenance_thread(apr_thread_t *thread, void *data)
{
    site_table_t * site_table = (site_table_t *) data;

    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, site_table->s, "site table maintenance thread starting.");

    while (1) {
        sleep(SITE_TABLE_POLL_INTERVAL);

        struct stat stat;
        int ret = fstat(site_table->fd, &stat);

        if (ret == -1) {
            ap_log_error(APLOG_MARK, APLOG_ERR, APR_FROM_OS_ERROR(errno), site_table->s, "Failed to stat site table file");
            continue;
        }

        if (site_table->modification_time < stat.st_mtime) {
            read_site_table(site_table);
        }

    }
    return NULL;
}

const site_t * site_table_lookup_site(site_table_t *site_table, const char *key)
{

    apr_status_t status = apr_thread_rwlock_wrlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_rdlock(site_table->lock);
    }

    site_t * site = apr_hash_get(site_table->site_configurations, key, APR_HASH_KEY_STRING);

    status = apr_thread_rwlock_unlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_unlock(site_table->lock);
    }

    return site;
}

const site_t * site_table_lookup_path(site_table_t *site_table, const char *key, const request_rec *r)
{

    apr_status_t status = apr_thread_rwlock_wrlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_rdlock(site_table->lock);
    }

    site_t * site = NULL;
    
    if (r->hostname) {
        const char * key0 = site_key(r->pool, key, r->hostname);

        site = apr_hash_get(site_table->path_table, key0, APR_HASH_KEY_STRING);
    }

    if (site == NULL) {
        site = apr_hash_get(site_table->path_table, key, APR_HASH_KEY_STRING);
    }

    status = apr_thread_rwlock_unlock(site_table->lock);

    while (status) {
        status = apr_thread_rwlock_unlock(site_table->lock);
    }

    return site;
}

static int check_path_exists(const site_table_t *site_table, const site_t *site)
{
    const site_t * check = apr_hash_get(site_table->path_table, site->script_path, APR_HASH_KEY_STRING);

    if (check != NULL) {
        const char * type = strcmp(check->script_path, site->script_path) == 0 ? "script" : "article";
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s, "The script path [%s] of site [%s] already exists as the [%s] path of [%s]!", site->script_path, site->name, type, check->name);
        return 1;
    }

    check = apr_hash_get(site_table->path_table, site->article_path, APR_HASH_KEY_STRING);

    if (check != NULL) {
        const char * type = strcmp(check->script_path, site->script_path) == 0 ? "script" : "article";
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, site_table->s, "The article path [%s] of site [%s] already exists as the [%s] path of [%s]!", site->script_path, site->name, type, check->name);
        return 1;
    }

    return 0;
}

