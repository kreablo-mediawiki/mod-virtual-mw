/*
 * Copyright 2012  Andreas Jonsson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SITE_TABLE_H
#define SITE_TABLE_H

#include <apr.h>
#include <apr_general.h>
#include <apr_hash.h>
#include <httpd.h>

#undef PACKAGE_BUGREPORT
#undef PACKAGE_NAME
#undef PACKAGE_STRING
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION

#ifndef SITE_TABLE_FILENAME
  #define SITE_TABLE_FILENAME "/srv/virtual_mw/site_table.yaml"
#endif
#define SITE_TABLE_POLL_INTERVAL 10

#define SITE_TABLE_DEFAULT_SITE "default_site"
#define SITE_TABLE_DEFAULT_VERSION "default_version"

typedef struct site_table site_table_t;

/**
 * The structure of a site in the table.
 */
typedef struct site
{
    /** The site name. */
    const char * name;
    /** The hostname for matching.  This is optional. */
    const char * hostname;    
    /** Corresponding to $wgArticlePath.  This is mandatory. */
    const char * article_path;
    /** Corresponding to $wgScriptPath.  This is mandatory. */
    const char * script_path;
    /** The address of the FPM process pool to forward requests to this site to.  Ignored for aliases. */
    const char * fpm_address;
    /** The target site of an alias.  NULL if the site is not an alias. */
    const char * alias_of;
    /** The version of the site.  Ignored for aliased sites */
    const char * version;
} site_t;

/**
 * Initiate the site table.
 * @param pool The apr pool to initialize this site table in.
 * @param s The list of server_rec.
 * @return The initiated site table or {@literal NULL} on failure.
 */
site_table_t * site_table_initialize(apr_pool_t * pool, server_rec *s);

/**
 * Look up a key in the site table.
 *
 * @param site_table The site table.
 * @param key The key (the site name) to lookup.
 * @return the corresponding site structure, or NULL if non-existing.
 */
const site_t * site_table_lookup_site(site_table_t *site_table, const char *key);

/**
 * Look up a key in the path table.
 *
 * @param site_table The site table.
 * @param key The key (the script path or article path) to lookup.
 * @param r The request.
 * @return the corresponding site structure, or NULL if non-existing.
 */
const site_t * site_table_lookup_path(site_table_t *site_table, const char *key, const request_rec *r);

/**
 * Obtain an iterator over sites.
 *
 * @param pool The apr pool to allocate from.
 * @param site_table The site table.
 * @return A hash index.
 */
apr_hash_index_t * site_table_site_iterator(apr_pool_t * pool, site_table_t * site_table);

#endif
