#!/usr/bin/perl -w

use strict;

use Getopt::Long;
use YAML::Any qw/LoadFile DumpFile/;
use Fcntl qw(:flock);

my %hostname_additions = ();
my %uri_part_additions = ();
my @hostname_removals  = ();
my @uri_part_removals  = ();
my %alias_additions    = ();
my @alias_removals     = ();
my $site_table_filename;

my $result = GetOptions("domain-name-additions=s" => \%hostname_additions,
                        "uri-part-additions=s"    => \%uri_part_additions,
                        "domain-name-removals=s"  => \@hostname_removals,
                        "uri-part-removals=s"     => \@uri_part_removals,
                        "site-table-filename=s"   => \$site_table_filename,
                        "alias-additions=s"       => \%alias_additions,
                        "alias-removals=s"        => \@alias_removals
    );


if (!$result) {
    exit 1;
}

if (!$site_table_filename) {
    die "The parameter --site-table-filename is mandatory.";
}

open SITE_TABLE_FILE, "<$site_table_filename";

flock(SITE_TABLE_FILE, LOCK_EX) or die "Failed to lock file: $!";

my %site_table_hash = read_site_table_file($site_table_filename);

unless (%site_table_hash) {
    %site_table_hash = ();
}

my $site_table = \%site_table_hash;

remove_removals($site_table, 'hostname', \@hostname_removals);
remove_removals($site_table, 'uri_part', \@uri_part_removals);
remove_removals($site_table, 'alias',  \@alias_removals);
add_additions($site_table,   'hostname', \%hostname_additions);
add_additions($site_table,   'uri_part', \%uri_part_additions);
add_additions($site_table,   'alias', \%alias_additions);

write_site_table_file($site_table_filename, $site_table);

flock(SITE_TABLE_FILE, LOCK_UN) or die "Failed to unlock file: $!";

sub write_site_table_file {
    my $filename = shift;
    my $site_table = shift;

    DumpFile($filename, %$site_table);
}

sub read_site_table_file {
    my $filename = shift;

    if (!open FILE, "<$filename") {
        print "overwriting file\n";
        open FILE, ">$filename";
        print FILE "--- ~\n";
    }

    close FILE;

    return LoadFile("$filename");
}

sub remove_removals {
    my($site_table, $table, $removals) = @_;

    if (!$site_table->{$table}) {
        $site_table->{$table} = {};
    }

    for my $key (@$removals) {
        delete $site_table->{$table}->{$key};
    }
}

sub add_additions {
    my($site_table, $table, $additions) = @_;

    if (!$site_table->{$table}) {
        $site_table->{$table} = {};
    }

    while (my ($key => $value) = each %$additions) {
        print "Adding $key => $value  to $table\n";
        $site_table->{$table}->{$key} = $value;
    }
}

