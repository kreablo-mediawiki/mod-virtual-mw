/* Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Much code copied from mod_speling.c - by Alexei Kosut <akosut@organic.com> June, 1996
 *
 * Sep-1999 Hugo Haas <hugo@w3.org>
 * o Added a CheckCaseOnly option to check only miscapitalized words.
 *
 * 08-Aug-1997 <Martin.Kraemer@Mch.SNI.De>
 * o Upgraded module interface to apache_1.3a2-dev API (more NULL's in
 *   speling_module).
 * o Integrated tcsh's "spelling correction" routine which allows one
 *   misspelling (character insertion/omission/typo/transposition).
 *   Rewrote it to ignore case as well. This ought to catch the majority
 *   of misspelled requests.
 * o Commented out the second pass where files' suffixes are stripped.
 *   Given the better hit rate of the first pass, this rather ugly
 *   (request index.html, receive index.db ?!?!) solution can be
 *   omitted.
 * o wrote a "kind of" html page for mod_speling
 *
 * Adapted for mod_virtual_mw by Andreas Jonsson <andreas.jonsson@kreablo.se> October, 2013
 */

#ifndef MOD_VIRTUAL_MW_CHECK_SPELING_H
#define MOD_VIRTUAL_MW_CHECK_SPELING_H

#include <apr_lib.h>
#include <apr_tables.h>
#include <apr_hash.h>

typedef enum {
    SP_IDENTICAL = 0,
    SP_MISCAPITALIZED = 1,
    SP_TRANSPOSITION = 2,
    SP_MISSINGCHAR = 3,
    SP_EXTRACHAR = 4,
    SP_SIMPLETYPO = 5,
    SP_VERYDIFFERENT = 6
} sp_reason;

typedef struct {
    const char *name;
    sp_reason quality;
} misspelled_article_path;

/*
 * spdist() is taken from Kernighan & Pike,
 *  _The_UNIX_Programming_Environment_
 * and adapted somewhat to correspond better to psychological reality.
 * (Note the changes to the return values)
 *
 * According to Pollock and Zamora, CACM April 1984 (V. 27, No. 4),
 * page 363, the correct order for this is:
 * OMISSION = TRANSPOSITION > INSERTION > SUBSTITUTION
 * thus, it was exactly backwards in the old version. -- PWP
 *
 * This routine was taken out of tcsh's spelling correction code
 * (tcsh-6.07.04) and re-converted to apache data types ("char" type
 * instead of tcsh's NLS'ed "Char"). Plus it now ignores the case
 * during comparisons, so is a "approximate strcasecmp()".
 * NOTE that is still allows only _one_ real "typo",
 * it does NOT try to correct multiple errors.
 */

static inline sp_reason spdist(const char *s, const char *t)
{
    for (; apr_tolower(*s) == apr_tolower(*t); t++, s++) {
        if (*t == '\0') {
            return SP_MISCAPITALIZED;   /* exact match (sans case) */
        }
    }
    if (*s) {
        if (*t) {
            if (s[1] && t[1] && apr_tolower(*s) == apr_tolower(t[1])
                && apr_tolower(*t) == apr_tolower(s[1])
                && strcasecmp(s + 2, t + 2) == 0) {
                return SP_TRANSPOSITION;        /* transposition */
            }
            if (strcasecmp(s + 1, t + 1) == 0) {
                return SP_SIMPLETYPO;   /* 1 char mismatch */
            }
        }
        if (strcasecmp(s + 1, t) == 0) {
            return SP_EXTRACHAR;        /* extra character */
        }
   }
    if (*t && strcasecmp(s, t + 1) == 0) {
        return SP_MISSINGCHAR;  /* missing character */
    }
    return SP_VERYDIFFERENT;    /* distance too large to fix. */
}

static inline int sort_by_quality(const void *left, const void *rite)
{
    return (int) (((misspelled_article_path *) left)->quality)
        - (int) (((misspelled_article_path *) rite)->quality);
}

static inline int check_speling(request_rec *r, site_table_t * site_table, const char * path, const char ** new)
{
    apr_array_header_t * candidates = apr_array_make(r->pool, 2, sizeof(misspelled_article_path));

    apr_hash_index_t * hi = site_table_site_iterator(r->pool, site_table);

    *new = NULL;

    misspelled_article_path * min = NULL;

    for (; hi; hi = apr_hash_next(hi)) {
        void * p;

        apr_hash_this(hi, NULL, NULL, &p);

        const site_t * site = p;

        sp_reason q;

        /*
         * This can only happen if there is a bug elsewhere in the
         * code.  But we decline to redirect to avoid infinite
         * redirection.
         */
        if (strcmp(path, site->article_path) == 0) {
            return DECLINED;
        }

         if (((q = spdist(path, site->article_path)) != SP_VERYDIFFERENT)) {
            misspelled_article_path *sp_new;

            sp_new = (misspelled_article_path *) apr_array_push(candidates);
            sp_new->name = apr_pstrdup(r->pool, site->article_path);
            sp_new->quality = q;

            if (min == NULL || sp_new->quality < min->quality) {
                min = sp_new;
            }
        }
    }

    if (min == NULL) {
        return DECLINED;
    }

    /*
     * We currently don't support multi choice, we just return
     * whatever comes out first among the most likely candidates.
     */
    *new = min->name;
    return OK;
}

#endif
